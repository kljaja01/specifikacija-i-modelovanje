from PySide2.QtWidgets import QToolBar, QAction,QMenu
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt

class FunctionToolbar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)

        self.setAllowedAreas(Qt.TopToolBarArea | Qt.BottomToolBarArea)

        fetch_nb = QAction("Fetch next block", self)
        fetch_nb.setIconText("Fetch next block")
        self.addAction(fetch_nb)


        add_record = QAction("Add record", self)
        add_record.setIconText("Add record")
        self.addAction(add_record)

        update_recors = QAction("Update record", self)
        update_recors.setIconText("Update record")
        self.addAction(update_recors)

        delete_record = QAction("Delete record", self)
        delete_record.setIconText("Delete record")
        self.addAction(delete_record)

        find_record = QAction("Find record", self)
        find_record.setIconText("Find record")
        self.addAction(find_record)
