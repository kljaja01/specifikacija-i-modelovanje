from PySide2.QtWidgets import QTreeView,QFileSystemModel,QDockWidget
from PySide2.QtCore import QDir

class FileExplorer(QTreeView):
    def __init__(self, parent=None):
        super().__init__(parent)

        
        model = QFileSystemModel()
        model.setRootPath("C:/Users/korisnik/Desktop/sims-projekat1.1/data")  

        
        self.setModel(model)

        
        self.setRootIndex(model.index("C:/Users/korisnik/Desktop/sims-projekat1.1/data"))
        