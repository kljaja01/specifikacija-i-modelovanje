from PySide2.QtWidgets import QWidget, QVBoxLayout, QTabWidget, QFileDialog, QAction, QMessageBox,QSplitter
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.data_view.data_view import DataView
class CentralWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.on_open_action = QAction(QIcon("resources/icons/folder-open-document.png"), "Open")
        self.on_open_action.triggered.connect(self.open_primary_table)

        self.cw_layout = QVBoxLayout(self)

        # create the top-level splitter
        splitter = QSplitter(self)


        left_splitter = QSplitter(self)
        left_splitter.setOrientation(Qt.Vertical)

        self.primary_tables_tab_widget = QTabWidget(self)
        self.secondary_tables_tab_widget = QTabWidget(self)

        self.primary_tables_tab_widget.setTabsClosable(True)
        self.secondary_tables_tab_widget.setTabsClosable(True)

        self.primary_tables_tab_widget.tabCloseRequested.connect(self.on_close_primary_tab)
        self.secondary_tables_tab_widget.tabCloseRequested.connect(self.secondary_tables_tab_widget.removeTab)


        self.third_table_tab_widget = QTabWidget(self)

        splitter.addWidget(left_splitter)
        splitter.addWidget(self.third_table_tab_widget)
        left_splitter.addWidget(self.primary_tables_tab_widget)
        left_splitter.addWidget(self.secondary_tables_tab_widget)

        self.cw_layout.addWidget(splitter)

        self.setLayout(self.cw_layout)
        # self.cw_layout = QVBoxLayout(self)

        # self.primary_tables_tab_widget = QTabWidget(self)
        # self.secondary_tables_tab_widget = QTabWidget(self)

        # self.primary_tables_tab_widget.setTabsClosable(True)
        # self.secondary_tables_tab_widget.setTabsClosable(True)


        # self.third_tables_tab_widget = QTabWidget(self)
        # self.third_tables_tab_widget.setTabsClosable(True)
        # self.third_tables_tab_widget.tabCloseRequested.connect(self.third_tables_tab_widget.removeTab)

        # self.primary_tables_tab_widget.tabCloseRequested.connect(self.on_close_primary_tab)
        # self.secondary_tables_tab_widget.tabCloseRequested.connect(self.secondary_tables_tab_widget.removeTab)

        # self.cw_layout.addWidget(self.primary_tables_tab_widget)
        # self.cw_layout.addSpacing(10)
        # self.cw_layout.addWidget(self.secondary_tables_tab_widget)

        # self.setLayout(self.cw_layout)

        self.parent().add_actions(where="toolbar", name=None, actions=self.export_actions())

    def open_primary_table(self):
        
        # 1. pronaci datoteka/folder workspace-a koji se otvara
        # 2. parsirati pomocu handle metode i kreirati model
        # 3. napraviti DataView (tabelarni prikaz)
        # 4. postaviti model DataView-u
        # 5. kreirati novi Tab u koji ce kao widget biti dodat DataView
        # 6. pronaci sve zavisnosti i kreirati nove tabove 
        # u secondary_tab_widget (koraci isti kao i za primary_tab_widget, koraci 2-5)
        # FIXME: demonstrativni primer, promeniti po sledecim koracima
        from gui.widgets.data_view.data_model import DataModel
        import csv
        file_name = QFileDialog.getOpenFileName(self, "Open data file", "/resources/data", "CSV files (*.csv)")
        # kada se otvori diajlog a ne izabere datoteka
        if file_name[0] == "":
            return
        # ucitavanje podataka iz datoteke
        with open(file_name[0], "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=";")
            reader = list(reader)
            header = reader[0] # FIXME: ovo ce biti u meta-podacima
            data = reader[1:]
            # kreiranje modela na osnovu ucitanih podataka
            table_model = DataModel(data=data, header_data=header)
            # postavljanje modela u prikaz
            data_view = DataView()
            data_view.table_view.setModel(table_model)
            self.primary_tables_tab_widget.addTab(data_view, file_name[0].split("/")[-1])
        
    def export_actions(self):
        return [self.on_open_action]
    

    def on_close_primary_tab(self, index):
        # TODO ako ima nekih promena pitati da li se zele one sacuvati itd.
        QMessageBox.warning(self.parent(), "Upozorenje za cuvanje podataka", "Podaci nisu sacuvani, ako zelite prvo ih sacuvajte pa zatvorite tab", QMessageBox.Ok)
        self.primary_tables_tab_widget.removeTab(index)