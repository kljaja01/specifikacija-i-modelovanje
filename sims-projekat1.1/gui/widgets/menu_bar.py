from PySide2.QtWidgets import QMenuBar,QMenu

class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.file_menu=QMenu("File",self)
        self.database_menu=QMenu("Database",self)
        self.help_menu=QMenu("Help",self)

        self._populate_menues()
        self._add_actions()

    def _add_actions(self):
        self.file_menu.addAction("Open")


        self.help_menu.addAction("Help")
        self.help_menu.addAction("About")






    def _populate_menues(self):
        self.addMenu(self.file_menu)
        self.addMenu(self.database_menu)
        self.addMenu(self.help_menu)
