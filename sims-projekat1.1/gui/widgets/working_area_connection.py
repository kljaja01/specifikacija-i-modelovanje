from PySide2.QtWidgets import QDockWidget, QLabel
from PySide2.QtCore import Qt

class WAConnection(QDockWidget):
    def __init__(self, parent=None):
        super().__init__("Radna zona", parent)
        self.setAllowedAreas(
           Qt.RightDockWidgetArea
        )

        self.workspace_label = QLabel("Ovde ide sadržaj radne zone")
        self.workspace_label.setStyleSheet("border: 1px solid black;")

        self.setWidget(self.workspace_label)
