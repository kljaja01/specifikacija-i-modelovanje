from PySide2.QtWidgets import QToolBar, QAction,QMenu
from PySide2.QtGui import QIcon

class Toolbar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)


        new_file = QAction("New file", self)
        new_file.setIconText("New file")
        self.addAction(new_file)


        save_file = QAction("Save file", self)
        save_file.setIconText("Save file")
        self.addAction(save_file)

        close_file = QAction("Close file", self)
        close_file.setIconText("Close file")
        self.addAction(close_file)

        close_all = QAction("Close all", self)
        close_all.setIconText("Close all")
        self.addAction(close_all)

        about_ui = QAction("About ui", self)
        about_ui.setIconText("About ui")
        self.addAction(about_ui)