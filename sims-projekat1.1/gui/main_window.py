import sys
from PySide2.QtWidgets import QApplication,QMainWindow,QPushButton,QDockWidget,QLabel,QWidget,QSplitter,QVBoxLayout
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.menu_bar import MenuBar
from gui.widgets.tool_bar import Toolbar
from gui.widgets.file_explorer import FileExplorer
from gui.widgets.central_widget import CentralWidget
from gui.widgets.db_explorer import DBExplorer
from gui.widgets.working_area_connection import WAConnection
from gui.widgets.function_tool_bar import FunctionToolbar

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("icons/administrator.png"))
        self.resize(1000, 800)

        self.menu_bar=MenuBar(self)
        self.setMenuBar(self.menu_bar)
        
        self.tool_bar=Toolbar(parent=self)
        self.addToolBar(self.tool_bar)
        
        self.file_explorer=FileExplorer(self)
        dock_widget = QDockWidget("File Explorer", self)
        dock_widget.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        dock_widget.setWidget(self.file_explorer)
        self.addDockWidget(Qt.LeftDockWidgetArea, dock_widget)

        self.db_explorer=DBExplorer(self)
        db_dock_widget=QDockWidget("DB Explorer",self)
        db_dock_widget.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.BottomDockWidgetArea)
        db_dock_widget.setWidget(self.db_explorer)
        self.addDockWidget(Qt.LeftDockWidgetArea, db_dock_widget)

        self.central_widget=CentralWidget(self)
        self.setCentralWidget(self.central_widget)




        self.function_tool_bar=FunctionToolbar(parent=self)
        self.addToolBarBreak()
        self.addToolBar(self.function_tool_bar)

    def add_actions(self, where="menu", name=None, actions=[]):
        if where == "menu":
            # dodati u meni sa nazivom name (file, edit...)
            self.menu_bar.add_actions(name, actions)
        elif where == "toolbar":
            # dodati akcije u toolbar nakon separatora
            self.tool_bar.addSeparator()
            self.tool_bar.addActions(actions)
        # self.wa_connection=WAConnection(self)
        # wa_connection_dock_widget=QDockWidget("",self)
        # wa_connection_dock_widget.setWidget(self.wa_connection)
        # self.addDockWidget(Qt.RightDockWidgetArea,wa_connection_dock_widget)