from PySide2.QtCore import QAbstractItemModel,QModelIndex


class WorkspaceModel(QAbstractItemModel):
    def __init__(self, parent=None,workspace=None):
        #korenski element
        self.workspace=workspace



        super().__init__(parent)

    def get_element(self, index: QModelIndex):
        """
        :param index: Indeks sa kojeg se dobavlja element (sadržaj)
        :return: Sadržaj iz hijerarhije workspace-a / workspace  (u slučaju da je indeks nevažeć)
        """
        if index.isValid():
            # Element iz matrice dobijamo spram reda i kolone indeksa
            element = index.internalPointer()
            if element:
                return element
        return self.table_data
    def index(self, row, column, parent=QModelIndex()):
        item=self.get_element(parent)
        if(row>=0) and (row < len(item.children())and item.children()[row]):
            return self.createIndex(row,column,item.children()[row])
        return QModelIndex()
    
    def parent(self,index):
        return super().parent(index)
    

    def rowCount(self, parent):
        item=self.get_element(parent)
        return len(item.children())
    

    def columnCount(self, parent):
        return super().columnCount(parent)
    

    def data(self, index, role):
        return super().data(index, role)