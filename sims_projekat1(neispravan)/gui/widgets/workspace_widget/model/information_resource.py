from abc import ABC, abstractmethod
from gui.widgets.workspace_widget.model.type_handler import TypeHandler


class InformationResource(TypeHandler,ABC):
    def __init__(self,name="",path=""):
        super().__init__()
        
        self.name=name
        self.path=path
    @abstractmethod
    def parent(self):
        '''
        Metoda koja vraca listu dece informacionog resursa 
        :return: lista prazna ili popunjena decom
        '''
        ...