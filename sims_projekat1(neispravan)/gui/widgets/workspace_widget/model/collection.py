from gui.widgets.workspace_widget.model.information_resource import InformationResource

class Collection(InformationResource):
    def __init__(self,name="",path="",metadata_path="",parent=None,content=[]):
        super().__init__(name,path)
        #workspace ili kolekcije kome ova kolekcija pripada
        self._parent=parent
        self.metadata_path=metadata_path
        self.content=content

    def parent(self):
        return super().parent()
    
    def children(self):
        return self.content
    