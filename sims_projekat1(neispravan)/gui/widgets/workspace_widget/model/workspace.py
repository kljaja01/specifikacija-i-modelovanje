from gui.widgets.workspace_widget.model.information_resource import InformationResource

class Workspace(InformationResource):
    def __init__(self,name="",path="",collections=[]):
        super().__init__()

        self.collections=collections

    def parent(self):
        return None 
    
    def children(self):
        return self.collections
    
    def handle(self):
        ...