from gui.widgets.workspace_widget.model.information_resource import InformationResource

class File(InformationResource):
    def __init__(self,name="",path="",metadata_path="",collection=None,):
        super().__init__()
        self.metadata_path=metadata_path
        self.collection=collection

    def parent(self):
        return self.collection
    

    def children(self):
        return []

    