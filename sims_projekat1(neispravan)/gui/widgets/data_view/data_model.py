from PySide2.QtCore import QAbstractTableModel, QModelIndex, Qt

class DataModel(QAbstractTableModel):
    def __init__(self, parent=None, header_data=None, data=None):
        super().__init__(parent)
        self.table_data = data
        self.header_data = header_data

    def get_headers(self):
        return self.header_data
    
    def get_row(self, row=0):
        # Metoda koja služi za dobavljanje podataka iz reda
        # Ako je prosleđeni red u okviru granica tabele (njenog broja redova)
        if self.table_data is not None:
            if row < len(self.table_data):
                data_row = self.table_data[row] # Lista elemenata
                if data_row:
                    return data_row
                
    def replace_data(self, row, data=[]):
        # Emitovati signal za promenu
        # Izvršiti promenu
        self.table_data[row] = data
        # Emitovati signal za kraj promene

    # Sopstvena metoda (ne redefiniše se)
    def get_element(self, index: QModelIndex):
        """
        :param index: Indeks sa kojeg se dobavlja element (sadržaj)
        :return: Sadržaj za datu ćeliju / cela lista (u slučaju da je indeks nevažeć)
        """
        if index.isValid():
            # Element iz matrice dobijamo spram reda i kolone indeksa
            element = self.table_data[index.row()][index.column()]
            if element:
                return element
        return self.table_data

    # Redefinisanje neophodnih metoda za pravljenje našeg modela
    def rowCount(self, parent=...):
        return len(self.table_data) # Koliko ima redova (podlista) u listi
    
    def columnCount(self, parent=...):
        return len(self.header_data)
    
    def data(self, index, role=...):
        element = self.get_element(index)
        if role == Qt.DisplayRole: # Qt.DecorationRole (za ikonice)
            return element # Sadržaj ćelije
        
    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header_data[section] # section == kolona
        elif orientation == Qt.Vertical:
            return super().headerData(section, orientation, role)
