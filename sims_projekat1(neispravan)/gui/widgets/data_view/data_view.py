from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView, QAction, QFileDialog, QAbstractItemView
from PySide2.QtGui import QIcon
from gui.widgets.data_view.data_model import DataModel
from gui.widgets.edit_dialog.edit_dialog import EditDialog
import csv


class DataView(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.widget_layout = QVBoxLayout()

        # Akcija za otvaranje datoteke
        self.on_open_file_action = QAction(QIcon("icons/folder.png"), "Otvori")
        self.on_open_file_action.triggered.connect(self.on_open_file)

        # Prikaz tabele
        self.table_view = QTableView()
        # Dozvoljena selekcija samo jednog reda u tabeli
        self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)
        # Dozvoljeno selektovanje cele vrste reda
        self.table_view.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Kad se dvaput klikne na red, otvori se dijalog za izmenu podataka
        self.table_view.doubleClicked.connect(self.open_edit_form)

        # Model ce se populisati kada se otvori datoteka
        # self.table_model = DataModel()
        # self.table_view.setModel(self.table_model)

        self.widget_layout.addWidget(self.table_view)
        self.setLayout(self.widget_layout)

    def export_actions(self):
        return [self.on_open_file_action]

    def on_open_file(self):
        # Izbora datoteke iz fajl sistema
        file_name = QFileDialog.getOpenFileName(self, "Otvori datoteku", "/prokelat_sims/data", "CSV datoteke (*.csv)")
        # Ako se otvori dijalog a ne izabere datoteka
        if file_name[0] == "":
            return
        # Ucitavanje podataka iz datoteke
        with open(file_name[0], "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=";")
            reader = list(reader)
            header = reader[0] # FIXME: ovo ce biti u meta-podacima
            data = reader[1:]
            # Kreiranje modela na osnovu ucitanih podataka
            self.table_model = DataModel(data=data, header_data=header)
            # Postavljanje modela u prikaz
            self.table_view.setModel(self.table_model)

    def open_edit_form(self, index=None):
        model = self.table_view.model()
        selected_labels = model.get_headers()
        selected_data = model.get_row(index.row())
        # Kreiranje dijaloga za izmenu podataka
        edit_dialog = EditDialog(self.parent())
        # Popunjavanje dijaloga podacima iz reda tabele
        edit_dialog.enter_data(selected_labels, selected_data)
        # Prikazivanje dijaloga
        result = edit_dialog.exec_()
        if result == 1: # Izmena prihvacena
            data = edit_dialog.get_data()
            model.replace_data(index.row(), data)
