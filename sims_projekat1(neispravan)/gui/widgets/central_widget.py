from PySide2.QtWidgets import QWidget, QVBoxLayout, QTextEdit
# from gui.widgets.table_widget.test_table_widget import TestTableWidget
from gui.widgets.data_view.data_view import DataView
import csv

class CentralWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.cw_layout = QVBoxLayout(self)
        self.table = DataView()


        self.parent().add_actions(actions=self.table.export_actions(),where="toolbar")

        self.cw_layout.addWidget(self.table)

        # obavezno dati layout uvezujemo na widget (CentralWidget)
        self.setLayout(self.cw_layout)
        
    # def load_data(self):
    #     with open('data\Studenti\Studenti.txt',"r",encoding="utf-8") as file:
    #         reader = csv.reader(file, delimiter='\t')
    #         data = [row for row in reader]

    #     return data
    
    