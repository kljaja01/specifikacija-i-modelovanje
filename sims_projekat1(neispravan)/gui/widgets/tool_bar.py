from PySide2.QtWidgets import QToolBar, QAction, QMessageBox
from PySide2.QtGui import QIcon

from gui.widgets.help_dialog.help_dialog import HelpDialog

class ToolBar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        self.actions_dict = {}
        # da bi dodali akcije u recnik
        self.default_actions()
        # da bi se uvezalo sta se poziva kada se akcija trigeruje
        self.bind_actions()


    def default_actions(self):
        # # open
        # open_action = QAction(QIcon("icons/folder.png"), "Open")
        # exit
        exit_action = QAction(QIcon("icons/remove.png"), "Exit")
        # help
        help_action = QAction(QIcon("icons/questions.png"), "Help")
        # about
        about_action = QAction(QIcon("icons/about.png"), "About")
        about_action.setStatusTip("About program")

        # dodavanje akcija u recnik
        # self.actions_dict["open"] = open_action
        self.actions_dict["exit"] = exit_action
        self.actions_dict["help"] = help_action
        self.actions_dict["about"] = about_action

        # dodavanje akcija u toolbar
        # self.addAction(open_action)
        self.addAction(exit_action)
        self.addSeparator()
        self.addAction(help_action)
        self.addAction(about_action)

    def bind_actions(self):

        self.actions_dict["about"].triggered.connect(self.on_about_action) 

        self.actions_dict["help"].triggered.connect(self.on_help_action)

    def on_about_action(self):
        
        QMessageBox.information(self.parent(), "About program", "Prototip predmetnog projekta iz SIMS-a.", QMessageBox.Ok)

        
    def on_help_action(self):
        dialog = HelpDialog(self.parent())
        result = dialog.exec_()
        print(result)

