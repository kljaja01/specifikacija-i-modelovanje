from PySide2.QtWidgets import QWidget, QVBoxLayout, QTextEdit,QTableWidget,QTableWidgetItem


class TableWidget(QTableWidget):
    def __init__(self, data, *args):
        super().__init__(*args)
        self.data = data

        self.setRowCount(len(self.data))
        self.setColumnCount(6)
        headers = ['S1', 'Broj', 'Prezime', 'Ime', 'Pol', '']
        self.setHorizontalHeaderLabels(headers)

        # Popunite tablicu s podacima
        for row, row_data in enumerate(self.data):
            row_data_str = ';'.join(row_data)
            row_data = row_data_str.split(';')
            for col, col_data in enumerate(row_data):
                
                item = QTableWidgetItem(str(col_data))
                self.setItem(row, col, item)


