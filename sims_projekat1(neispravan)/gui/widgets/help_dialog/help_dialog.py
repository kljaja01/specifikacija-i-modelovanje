from PySide2.QtWidgets import QDialog, QVBoxLayout, QLabel, QDialogButtonBox
from PySide2.QtGui import QIcon

class HelpDialog(QDialog):
    def __init__(self, parent=None):
        # parent nam treba za kreiranje modalnog dijaloga
        super().__init__(parent)

        self.setWindowTitle("Help")
        self.setWindowIcon(QIcon("icons/questions.png"))
        self.resize(200, 150)

        self.dialog_layout = QVBoxLayout()

        self.help_text = QLabel("Vise detalja na linku: https://gitlab.com/kljaja01/specifikacija-i-modelovanje.git", self)

        # dugmici
        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        # populisanje layout-a

        self.dialog_layout.addWidget(self.help_text)
        self.dialog_layout.addWidget(self.button_box)
        
        self.setLayout(self.dialog_layout)


    