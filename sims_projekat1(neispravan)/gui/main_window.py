from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.central_widget import CentralWidget
from gui.widgets.menu_bar import MenuBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.workspace_widget.workspace_widget import StructureDockWidget


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        # poziv super inicijalizaotra (QMainWindow)
        super().__init__(parent)
        # Osnovna podesavanja glavnog prozora
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("icons/administrator.png"))
        self.resize(1000, 800)

        self.tool_bar = ToolBar(parent=self)
        self.menu_bar = MenuBar(self)
        self.file_dock_widget = StructureDockWidget("Struktura radnog prostora", self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)

        self.addToolBar(self.tool_bar)
        self.setMenuBar(self.menu_bar)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)



        self.status_bar.addWidget(QLabel("Ulogovani korisnik: " + "Luka Kljajic"))






    def get_actions(self):
        return self.tool_bar.actions_dict
    
    def add_actions(self,where="menu",name=None,actions=[]):
        if where=="menu":

            self.menu_bar.add_actions(name, actions)
        elif where == "toolbar":
            # dodati akcije u toolbar nakon separatora
            self.tool_bar.addSeparator()
            self.tool_bar.addActions(actions)