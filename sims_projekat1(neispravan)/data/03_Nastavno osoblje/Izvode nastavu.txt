S1;SII001;1;S0;2;P;1;1
S1;SII001;2;S0;6;R;1;1
S0;SII001;1;S0;1;P;1;1
S2;SII001;1;S0;1;P;1;1
(
   VU_OZNAKA            char(2) not null,
   NP_OZNAKA            varchar(6) not null,
   BROJ_AKTIVNOSTI      smallint not null,
   VU_NASTAVNIKA        char(2) not null,
   NASTAVNIK            int not null,
   VRSTA                char(1) not null default 'P',
   ISPITUJE_            bool not null default 0,
   AKTUELAN_            bool not null,
   primary key (VU_OZNAKA, NP_OZNAKA, BROJ_AKTIVNOSTI)
);