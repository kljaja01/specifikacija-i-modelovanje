/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     1/6/2023 10:46:41 AM                         */
/*==============================================================*/


drop table if exists BAZEN_IZBORNIH;

drop table if exists DRZAVA;

drop table if exists ISTORIJA_POLAGANJA;

drop table if exists IZVODE_NASTAVU;

drop table if exists NASELJENO_MESTO;

drop table if exists NASTAVNICI;

drop table if exists NASTAVNI_PREDMET;

drop table if exists NIVO_STUDIJA;

drop table if exists OBAVEZE_STUDENTA;

drop table if exists PLAN_STUDIJSKE_GRUPE;

drop table if exists SASTAV_DRZAVE;

drop table if exists STRUKTURA_USTANOVE;

drop table if exists STUDENTI;

drop table if exists STUDIJSKI_PROGRAMI;

drop table if exists TOK_STUDIJA;

drop table if exists VISOKOSKOLSKA_USTANOVA;

/*==============================================================*/
/* Table: BAZEN_IZBORNIH                                        */
/*==============================================================*/
create table BAZEN_IZBORNIH
(
   VU_OZNAKA            char(2) not null,
   NP_OZNAKA            varchar(6) not null,
   REDNI_BROJ           numeric(2,0) not null,
   NAS_NP_OZNAKA        varchar(6) not null,
   primary key (VU_OZNAKA, NP_OZNAKA, REDNI_BROJ)
);

/*==============================================================*/
/* Table: DRZAVA                                                */
/*==============================================================*/
create table DRZAVA
(
   DR_OZNAKA            char(3) not null,
   DR_NAZIV             varchar(120) not null,
   NM_OZNAKA            int,
   DRZ_DR_OZNAKA        char(3),
   primary key (DR_OZNAKA)
);

/*==============================================================*/
/* Table: ISTORIJA_POLAGANJA                                    */
/*==============================================================*/
create table ISTORIJA_POLAGANJA
(
   STU_VU_OZNAKA        char(2) not null,
   STU_BROJ_INDEKSA     bigint not null,
   BROJ_POLAGANJA       int not null,
   POLAGAO_KOD          int,
   NP_OZNAKA            varchar(6),
   KADA                 date not null,
   BODOVA               decimal(3,2) not null,
   OCENA                numeric(2,0) not null,
   ECTS                 numeric(2,0) not null default 0,
   primary key (STU_VU_OZNAKA, STU_BROJ_INDEKSA, BROJ_POLAGANJA)
);

/*==============================================================*/
/* Table: IZVODE_NASTAVU                                        */
/*==============================================================*/
create table IZVODE_NASTAVU
(
   VU_OZNAKA            char(2) not null,
   NP_OZNAKA            varchar(6) not null,
   BROJ_AKTIVNOSTI      smallint not null,
   NASTAVNIK            int not null,
   VRSTA                char(1) not null default 'P',
   ISPITUJE_            bool not null default 0,
   AKTUELAN_            bool not null,
   primary key (VU_OZNAKA, NP_OZNAKA, BROJ_AKTIVNOSTI)
);

/*==============================================================*/
/* Table: NASELJENO_MESTO                                       */
/*==============================================================*/
create table NASELJENO_MESTO
(
   DR_OZNAKA            char(3) not null,
   NM_OZNAKA            int not null,
   NM_NAZIV             varchar(60) not null,
   NM_PTT_OZNAKA        varchar(12),
   primary key (DR_OZNAKA, NM_OZNAKA)
);

alter table NASELJENO_MESTO comment 'Registar naselja u sklopu odabrane dr�ave.';

/*==============================================================*/
/* Table: NASTAVNICI                                            */
/*==============================================================*/
create table NASTAVNICI
(
   VU_OZNAKA            char(2) not null,
   IDENT                int not null,
   NAS_PREZIME          varchar(20) not null,
   NAS_IME_RODITELJA    varchar(20),
   NAS_IME              varchar(20) not null,
   NAS_JMBG             char(13),
   NAS_POL              char(1) not null default 'N',
   NAS_TELEFON          varchar(20),
   NAS_ADRESA_STANOVANJA varchar(80),
   DR_OZNAKA            char(3),
   NM_OZNAKA            int,
   primary key (VU_OZNAKA, IDENT)
);

/*==============================================================*/
/* Table: NASTAVNI_PREDMET                                      */
/*==============================================================*/
create table NASTAVNI_PREDMET
(
   VU_OZNAKA            char(2) not null,
   NP_OZNAKA            varchar(6) not null,
   NP_NAZIV             varchar(120) not null,
   NP_ESPB              numeric(2,0) not null,
   IZBORNA_POZICIJA     bool not null default 0,
   BIRA_SE              numeric(2,0),
   primary key (VU_OZNAKA, NP_OZNAKA)
);

/*==============================================================*/
/* Table: NIVO_STUDIJA                                          */
/*==============================================================*/
create table NIVO_STUDIJA
(
   NIV_OZNAKA           numeric(2,0) not null,
   NIV_NAZIV            varchar(80) not null,
   primary key (NIV_OZNAKA)
);

/*==============================================================*/
/* Table: OBAVEZE_STUDENTA                                      */
/*==============================================================*/
create table OBAVEZE_STUDENTA
(
   STU_VU_OZNAKA        char(2) not null,
   STU_BROJ_INDEKSA     bigint not null,
   BROJ_OBAVEZE         smallint not null,
   SLUSA_KOD            int not null,
   NP_OZNAKA            varchar(6) not null,
   OD_KADA              date,
   primary key (STU_VU_OZNAKA, STU_BROJ_INDEKSA, BROJ_OBAVEZE)
);

/*==============================================================*/
/* Table: PLAN_STUDIJSKE_GRUPE                                  */
/*==============================================================*/
create table PLAN_STUDIJSKE_GRUPE
(
   STU_VU_OZNAKA        char(2) not null,
   SP_OZNAKA            varchar(3) not null,
   SPB_BLOK             numeric(2,0) not null,
   SPB_POZICIJA         numeric(2,0) not null,
   NP_OZNAKA            varchar(6) not null,
   primary key (STU_VU_OZNAKA, SP_OZNAKA, SPB_BLOK, SPB_POZICIJA)
);

/*==============================================================*/
/* Table: SASTAV_DRZAVE                                         */
/*==============================================================*/
create table SASTAV_DRZAVE
(
   DRZ_DR_OZNAKA        char(3) not null,
   DS_POZCIJA           numeric(3,0) not null,
   DR_OZNAKA            char(3) not null,
   DS_OD                date,
   DS_DO                date,
   primary key (DRZ_DR_OZNAKA, DS_POZCIJA)
);

alter table SASTAV_DRZAVE comment 'Omogu�ava definisanje strukture ili sastava slo�ene dr�ave u';

/*==============================================================*/
/* Table: STRUKTURA_USTANOVE                                    */
/*==============================================================*/
create table STRUKTURA_USTANOVE
(
   VIS_VU_OZNAKA       char(2) not null,
   VU_OZNAKA            char(2) not null,
   primary key (VIS_VU_OZNAKA, VU_OZNAKA)
);

/*==============================================================*/
/* Table: STUDENTI                                              */
/*==============================================================*/
create table STUDENTI
(
   VU_OZNAKA            char(2) not null,
   STU_BROJ_INDEKSA     bigint not null,
   STU_PREZIME          varchar(20) not null,
   STU_IME_RODITELJA    varchar(20),
   STU_IME              varchar(20) not null,
   STU_POL              char(1) not null default 'N',
   STU_TELEFON          varchar(20),
   STU_JMBG             char(13),
   STU_DATUM_RODJENJA   date,
   STU_ADRESA_STANOVANJA varchar(80),
   DR_OZNAKA            char(3),
   NM_OZNAKA            int,
   primary key (VU_OZNAKA, STU_BROJ_INDEKSA)
);

/*==============================================================*/
/* Table: STUDIJSKI_PROGRAMI                                    */
/*==============================================================*/
create table STUDIJSKI_PROGRAMI
(
   VU_OZNAKA            char(2) not null,
   SP_OZNAKA            varchar(3) not null,
   SP_NAZIV             varchar(120) not null,
   NIV_OZNAKA           numeric(2,0) not null,
   primary key (VU_OZNAKA, SP_OZNAKA)
);

/*==============================================================*/
/* Table: TOK_STUDIJA                                           */
/*==============================================================*/
create table TOK_STUDIJA
(
   STU_VU_OZNAKA        char(2) not null,
   STU_BROJ_INDEKSA     bigint not null,
   BROJ_UPISA           numeric(2,0) not null,
   SP_OZNAKA            varchar(3) not null,
   TOK_SKOLSKA_GODINA   numeric(4,0) not null,
   TOK_GODINA_STUDIJA   numeric(1,0) not null,
   TOK_BLOK             numeric(2,0) not null,
   TOK_DATUM_UPISA      date not null,
   TOK_DATUM_OVERE      date,
   TOK_ESPB_POCETNI     numeric(3,0) not null default 0,
   TOK_ESPB_KRAJNJI     numeric(3,0) not null,
   primary key (STU_VU_OZNAKA, STU_BROJ_INDEKSA, BROJ_UPISA)
);

/*==============================================================*/
/* Table: VISOKOSKOLSKA_USTANOVA                                */
/*==============================================================*/
create table VISOKOSKOLSKA_USTANOVA
(
   VU_OZNAKA            char(2) not null,
   VU_NAZIV             varchar(80) not null,
   VU_ADRESA            varchar(80) not null,
   DR_OZNAKA            char(3) not null,
   NM_OZNAKA            int not null,
   primary key (VU_OZNAKA)
);

alter table BAZEN_IZBORNIH add constraint FK_IZBORNA_POZICIJA foreign key (VU_OZNAKA, NP_OZNAKA)
      references NASTAVNI_PREDMET (VU_OZNAKA, NP_OZNAKA) on delete restrict on update restrict;

alter table BAZEN_IZBORNIH add constraint FK_IZBORNI_PREDMET foreign key (VU_OZNAKA, NAS_NP_OZNAKA)
      references NASTAVNI_PREDMET (VU_OZNAKA, NP_OZNAKA) on delete restrict on update restrict;

alter table DRZAVA add constraint FK_GLAVNI_GRAD foreign key (DR_OZNAKA, NM_OZNAKA)
      references NASELJENO_MESTO (DR_OZNAKA, NM_OZNAKA) on delete restrict on update restrict;

alter table DRZAVA add constraint FK_PRAVNI_NASLEDNIK foreign key (DRZ_DR_OZNAKA)
      references DRZAVA (DR_OZNAKA) on delete restrict on update restrict;

alter table ISTORIJA_POLAGANJA add constraint FK_POLAGANJA foreign key (STU_VU_OZNAKA, STU_BROJ_INDEKSA)
      references STUDENTI (VU_OZNAKA, STU_BROJ_INDEKSA) on delete restrict on update restrict;

alter table ISTORIJA_POLAGANJA add constraint FK_POLAGAO foreign key (STU_VU_OZNAKA, NP_OZNAKA)
      references NASTAVNI_PREDMET (VU_OZNAKA, NP_OZNAKA) on delete restrict on update restrict;

alter table ISTORIJA_POLAGANJA add constraint FK_POLAGAO_KOD foreign key (STU_VU_OZNAKA, POLAGAO_KOD)
      references NASTAVNICI (VU_OZNAKA, IDENT) on delete restrict on update restrict;

alter table IZVODE_NASTAVU add constraint FK_KO_IZVODI_NASTAVU foreign key (VU_OZNAKA, NP_OZNAKA)
      references NASTAVNI_PREDMET (VU_OZNAKA, NP_OZNAKA) on delete restrict on update restrict;

alter table IZVODE_NASTAVU add constraint FK_NASTAVNIK_NA_PREDMETU foreign key (VU_OZNAKA, NASTAVNIK)
      references NASTAVNICI (VU_OZNAKA, IDENT) on delete restrict on update restrict;

alter table NASELJENO_MESTO add constraint FK_MESTA_U_DRZAVI foreign key (DR_OZNAKA)
      references DRZAVA (DR_OZNAKA) on delete restrict on update restrict;

alter table NASTAVNICI add constraint FK_MESTO_NASTAVNIKA foreign key (DR_OZNAKA, NM_OZNAKA)
      references NASELJENO_MESTO (DR_OZNAKA, NM_OZNAKA) on delete restrict on update restrict;

alter table NASTAVNICI add constraint FK_PREDAJU foreign key (VU_OZNAKA)
      references VISOKOSKOLSKA_USTANOVA (VU_OZNAKA) on delete restrict on update restrict;

alter table NASTAVNI_PREDMET add constraint FK_IZVODI_PREDMETE foreign key (VU_OZNAKA)
      references VISOKOSKOLSKA_USTANOVA (VU_OZNAKA) on delete restrict on update restrict;

alter table OBAVEZE_STUDENTA add constraint FK_OBAVEZE foreign key (STU_VU_OZNAKA, STU_BROJ_INDEKSA)
      references STUDENTI (VU_OZNAKA, STU_BROJ_INDEKSA) on delete restrict on update restrict;

alter table OBAVEZE_STUDENTA add constraint FK_PREDMET_OBAVEZA foreign key (STU_VU_OZNAKA, NP_OZNAKA)
      references NASTAVNI_PREDMET (VU_OZNAKA, NP_OZNAKA) on delete restrict on update restrict;

alter table OBAVEZE_STUDENTA add constraint FK_SLUSAO_KOD foreign key (STU_VU_OZNAKA, SLUSA_KOD)
      references NASTAVNICI (VU_OZNAKA, IDENT) on delete restrict on update restrict;

alter table PLAN_STUDIJSKE_GRUPE add constraint FK_NA_POZICIJI foreign key (STU_VU_OZNAKA, NP_OZNAKA)
      references NASTAVNI_PREDMET (VU_OZNAKA, NP_OZNAKA) on delete restrict on update restrict;

alter table PLAN_STUDIJSKE_GRUPE add constraint FK_STRUKTURA_PO_BLOKOVIMA foreign key (STU_VU_OZNAKA, SP_OZNAKA)
      references STUDIJSKI_PROGRAMI (VU_OZNAKA, SP_OZNAKA) on delete restrict on update restrict;

alter table SASTAV_DRZAVE add constraint FK_SLOZENA foreign key (DRZ_DR_OZNAKA)
      references DRZAVA (DR_OZNAKA) on delete restrict on update restrict;

alter table SASTAV_DRZAVE add constraint FK_U_NJENOM_SASTAVU foreign key (DR_OZNAKA)
      references DRZAVA (DR_OZNAKA) on delete restrict on update restrict;

alter table STRUKTURA_USTANOVE add constraint FK_MATICNA foreign key (VU_OZNAKA)
      references VISOKOSKOLSKA_USTANOVA (VU_OZNAKA) on delete restrict on update restrict;

alter table STRUKTURA_USTANOVE add constraint FK_REFERENCE_30 foreign key (VIS_VU_OZNAKA2)
      references VISOKOSKOLSKA_USTANOVA (VU_OZNAKA);

alter table STRUKTURA_USTANOVE add constraint FK_U_SASTAVU foreign key (VU_OZNAKA)
      references VISOKOSKOLSKA_USTANOVA (VU_OZNAKA) on delete restrict on update restrict;

alter table STUDENTI add constraint FK_MESTOSTANOVANJA foreign key (DR_OZNAKA, NM_OZNAKA)
      references NASELJENO_MESTO (DR_OZNAKA, NM_OZNAKA) on delete restrict on update restrict;

alter table STUDENTI add constraint FK_STUDIRAJU_NA foreign key (VU_OZNAKA)
      references VISOKOSKOLSKA_USTANOVA (VU_OZNAKA) on delete restrict on update restrict;

alter table STUDIJSKI_PROGRAMI add constraint FK_KLASIFIKACIJA_PO_NIVOU foreign key (NIV_OZNAKA)
      references NIVO_STUDIJA (NIV_OZNAKA) on delete restrict on update restrict;

alter table STUDIJSKI_PROGRAMI add constraint FK_OBRAZUJE_ZA foreign key (VU_OZNAKA)
      references VISOKOSKOLSKA_USTANOVA (VU_OZNAKA) on delete restrict on update restrict;

alter table TOK_STUDIJA add constraint FK_KO_STUDIRA foreign key (STU_VU_OZNAKA, SP_OZNAKA)
      references STUDIJSKI_PROGRAMI (VU_OZNAKA, SP_OZNAKA) on delete restrict on update restrict;

alter table TOK_STUDIJA add constraint FK_STUDIRANJE foreign key (STU_VU_OZNAKA, STU_BROJ_INDEKSA)
      references STUDENTI (VU_OZNAKA, STU_BROJ_INDEKSA) on delete restrict on update restrict;

alter table VISOKOSKOLSKA_USTANOVA add constraint FK_SEDISTE_USTANOVE foreign key (DR_OZNAKA, NM_OZNAKA)
      references NASELJENO_MESTO (DR_OZNAKA, NM_OZNAKA) on delete restrict on update restrict;

