(
   STU_VU_OZNAKA        char(2) not null,
   STU_BROJ_INDEKSA     bigint not null,
   BROJ_POLAGANJA       int not null,
   POLAGAO_KOD          int,
   NP_OZNAKA            varchar(6),
   KADA                 date not null,
   BODOVA               decimal(3,2) not null,
   OCENA                numeric(2,0) not null,
   ECTS                 numeric(2,0) not null default 0,
   primary key (STU_VU_OZNAKA, STU_BROJ_INDEKSA, BROJ_POLAGANJA)
);